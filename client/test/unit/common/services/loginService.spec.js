/* jshint undef:false*/
(function() {
  'use strict';

  describe("Service: loginService", function() {
  var loginService, $rootScope, returnedValue;
  beforeEach(module('app'));
  beforeEach(module('common'));
  beforeEach(inject(function(_LoginService_, _$rootScope_) {
    loginService = _LoginService_;
    $rootScope = _$rootScope_;
  }));
  describe('call login function with real user',function(){
    it("should promise me user", function() {
      var mockupUser = {username: 'Armedia', password: 'Armedia123'};

      loginService.userLogin(mockupUser).then(function (data){
        returnedValue = data;
      });

      $rootScope.$digest();

      expect(returnedValue).toEqual(mockupUser);
    });
  });
  describe('call login function with user with bad credentials',function(){
    it("should promise me error cause of wrong credentials", function() {
      var mockupUser = {username: 'Armedia1', password: 'Armedia123'};

      loginService.userLogin(mockupUser).then(function (data){
        //returnedValue = data;
      }, function(error){
        returnedValue = error;
      });

      $rootScope.$digest();

      expect(returnedValue).toEqual({success: false});
    });
  });
});
})();
