/* jshint undef:false*/
(function() {
  'use strict';

describe("Service: SessionService", function() {
    var sessionService, $rootScope;
    var mockupUser = {username: 'Armedia', password: 'Armedia123'};

    beforeEach(module('app'));
    beforeEach(module('common'));
    beforeEach(inject(function (_SessionService_, _$rootScope_) {
      sessionService = _SessionService_;
      $rootScope = _$rootScope_;
    }));

    describe('call getUser function', function () {
      beforeEach(function(){
        sessionService.loggedUser = mockupUser;
      });
      it('should return user', function () {
        var user = sessionService.getUser();

        expect(user).toEqual("Armedia");
      });
      afterEach(function(){
        sessionService.loggedUser = null;
      });
    });

    describe('call setUser function', function () {
      beforeEach(function(){
        sessionService.loggedUser = mockupUser;
      });
      it('should set user', function () {
        sessionService.setUser(mockupUser);

        expect(sessionService.loggedUser.username).toEqual("Armedia");
      });
      afterEach(function(){
        sessionService.loggedUser = null;
      });
    });

   describe('call isAuthentication function with true', function () {
      beforeEach(function(){
        sessionService.loggedUser = mockupUser;
      });
      it('should return true', function () {
        var user = sessionService.isAuthenticated();

        expect(user).toEqual(true);
      });
      afterEach(function(){
        sessionService.loggedUser = null;
      });
    });

   describe('call isAuthentication function with false', function () {
      beforeEach(function(){
        sessionService.loggedUser = null;
      });
      it('should return false', function () {
        var user = sessionService.isAuthenticated();

        expect(user).toEqual(false);
      });
      afterEach(function(){
        sessionService.loggedUser = null;
      });
  });
});
})();
