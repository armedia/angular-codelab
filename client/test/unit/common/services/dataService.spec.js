/* jshint undef:false*/
(function() {
  'use strict';

describe("Service: dataService", function() {
  var dataService, $rootScope, $httpBackend;
  beforeEach(module('app'));
  beforeEach(module('common'));
  beforeEach(inject(function(_DataService_, _$rootScope_, _$httpBackend_) {
    dataService = _DataService_;
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
  }));
  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('call getUsers function with success callback',function(){
    it('should get users with success', function() {
      $httpBackend.expect('GET', 'assets/home/users.json').respond(200);

      var returnedData = dataService.getUsers();
      $httpBackend.flush();

      var successHandler = jasmine.createSpy('success');
      var errorHandler = jasmine.createSpy('error');

      returnedData.then(successHandler, errorHandler).finally(function() {
        expect(successHandler).toHaveBeenCalled();
        expect(errorHandler).not.toHaveBeenCalled();
      });
    });
  });
  describe('call getUsers function with error callback',function(){
    it('should not get users', function() {
      $httpBackend.expect('GET', 'assets/home/users.json').respond(500);

      var returnedData = dataService.getUsers();
      $httpBackend.flush();

      var successHandler = jasmine.createSpy('success');
      var errorHandler = jasmine.createSpy('error');

      returnedData.then(successHandler, errorHandler).finally(function() {
        expect(successHandler).not.toHaveBeenCalled();
        expect(errorHandler).toHaveBeenCalled();
      });
    });
  });
});
})();
