/* jshint undef:false*/
(function() {
  'use strict';

  describe("Service: ConversationService", function() {
    var conversationService, $rootScope, returnedValue;
    var mockUserId = 1;
    var mockReturnValue = [{
      loggedUser: 'How are you today Vanessa?',
      timeLoggedUser: '12.30',
      user: 'Good, thank you. And  you?',
      timeUser: '12.32'
    }, {
      loggedUser: 'I am fine Vanessa, tnx for asking?', timeLoggedUser: '12.32', user: 'Nice.', timeUser: '12.35'
    }];
    beforeEach(module('app'));
    beforeEach(module('common'));
    beforeEach(inject(function (_ConversationService_, _$rootScope_) {
      conversationService = _ConversationService_;
      $rootScope = _$rootScope_;
    }));

    describe('call getConversation function', function () {
      it('should return conversation by given id', function () {
        returnedValue = conversationService.getConversation(mockUserId);

        expect(returnedValue).toEqual(mockReturnValue);
      });
    });
});
})();
