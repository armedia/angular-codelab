/* jshint undef:false*/
(function() {
  'use strict';

  describe("Directive: userProfile", function() {

    var scope, directiveScope, element, modal, actualOptions, parentModalInstance;

    var mockSelectedUser = {
      "id": 1,
      "icon": "assets/images/online.png",
      "name": "Vanessa Angel",
      "avatar": "assets/images/vanessa_avatar.png"
    };

    var fakeModal = {
      result: {
        then: function(confirmCallback) {
          //Store the callbacks for later when the user clicks on the OK button of the dialog
          this.confirmCallBack = confirmCallback;
        }
      },
      close: function( item ) {
        //The user clicked OK on the modal dialog, call the stored confirm callback with the selected item
        this.result.confirmCallBack();
      }
    };

    beforeEach(module('app'));
    beforeEach(module('home'));

    beforeEach(inject(function($rootScope, $compile, $uibModal) {
      scope = $rootScope.$new();
      modal = $uibModal;
      scope.parentModalInstance = jasmine.createSpyObj('modalInstance', ['dismiss']);
      scope.user = mockSelectedUser;

      element = angular.element('<user-profile user-profile="user" parent-modal-instance="parentModalInstance"></user-profile>');
      element = $compile(element)(scope);

      $rootScope.$digest();

      directiveScope = element.isolateScope();


    }));
    describe('call openUserModal function', function () {
      beforeEach(function(){
        spyOn(modal, 'open').and.callFake(function(options){
          actualOptions = options;
          return fakeModal;
        })
      });
      it("should call openUserModal function", function() {
        directiveScope.openUserModal(mockSelectedUser);

        expect(actualOptions.resolve.user()).toEqual(mockSelectedUser);
        expect(directiveScope.parentModalInstance.dismiss).toHaveBeenCalledWith('cancel');
        directiveScope.userModalInstance.close();
      });
    });
  });
})();
