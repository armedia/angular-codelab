/* jshint undef:false*/
(function() {
  'use strict';

describe("Directive: userList", function() {

  var scope, directiveScope, element, modal, actualOptions;
  var mockSelectedUser = {
    "id": 1,
    "icon": "assets/images/online.png",
    "name": "Vanessa Angel",
    "avatar": "assets/images/vanessa_avatar.png"
  };
  var mockupUsers = [

    {
      "id": 1,
      "icon": "assets/images/online.png",
      "name": "Vanessa Angel",
      "avatar": "assets/images/vanessa_avatar.png"
    },
    {
      "id": 2,
      "icon": "assets/images/online.png",
      "name": "Tom Baker",
      "avatar": "assets/images/tom_avatar.png"
    },
    {
      "id": 3,
      "icon": "assets/images/online.png",
      "name": "Eliza Bennett",
      "avatar": "assets/images/eliza_avatar.png"
    },
    {
      "id": 4,
      "icon": "assets/images/online.png",
      "name": "Martin Benson",
      "avatar": "assets/images/martin_avatar.png"
    },
    {
      "id": 5,
      "icon": "assets/images/online.png",
      "name": "Richard Briers",
      "avatar": "assets/images/richard_avatar.png"
    }
    ,
    {
      "id": 6,
      "icon": "assets/images/online.png",
      "name": "Kim Cattrall",
      "avatar": "assets/images/kim_avatar.png"
    }
    ,
    {
      "id": 7,
      "icon": "assets/images/online.png",
      "name": "Michael Crawford",
      "avatar": "assets/images/michael_avatar.png"
    }
    ,
    {
      "id": 8,
      "icon": "assets/images/online.png",
      "name": "Anthony Dawson",
      "avatar": "assets/images/anthony_avatar.png"
    }
  ];
  var fakeModal = {
    result: {
      then: function(confirmCallback) {
        //Store the callbacks for later when the user clicks on the OK button of the dialog
        this.confirmCallBack = confirmCallback;
      }
    },
    close: function( item ) {
      //The user clicked OK on the modal dialog, call the stored confirm callback with the selected item
      this.result.confirmCallBack();
    }
  };

  beforeEach(module('app'));
  beforeEach(module('home'));

  beforeEach(inject(function($rootScope, $compile, $uibModal) {
    scope = $rootScope.$new();
    modal = $uibModal;
    scope.user = mockupUsers;


    element = angular.element('<user-list user-list="user"></user-list>');
    element = $compile(element)(scope);

    $rootScope.$digest();

    directiveScope = element.isolateScope();


  }));
  describe('call openUserModal function', function () {
    beforeEach(function(){
      spyOn(modal, 'open').and.callFake(function(options){
        actualOptions = options;
        return fakeModal;
      })
    });
    it("should call openUserModal function", function() {
      directiveScope.openUserModal(mockSelectedUser);

      expect(actualOptions.resolve.user()).toEqual(mockSelectedUser);
      directiveScope.userModalInstance.close();
    });
  });

  describe('call openUserProfile function', function () {
    beforeEach(function(){
      spyOn(modal, 'open').and.callFake(function(options){
        actualOptions = options;
        return fakeModal;
      })
    });
    it("should call openUserProfile function", function() {
      directiveScope.openUserProfile(mockSelectedUser);

      expect(actualOptions.resolve.user()).toEqual(mockSelectedUser);
      directiveScope.userProfileModalInstance.close();
    });
  });
});
})();
