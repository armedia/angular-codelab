/* jshint undef:false*/
(function() {
  'use strict';

  describe('ErrorModalController', function() {
    var errorModalController, rootScope, scope, $uibModalInstance;

    beforeEach(module('app'));
    beforeEach(module('home'));
    beforeEach(inject(function($rootScope, $controller) {
      rootScope = $rootScope;
      scope = rootScope.$new();
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
      errorModalController = $controller('ErrorModalController', {
        $scope : scope,
        $uibModalInstance : $uibModalInstance
      });
    }));
    describe('confirmModal function',function(){
      it("should call confirmModal", function() {
        errorModalController.confirmModal();

        expect($uibModalInstance.close).toHaveBeenCalled();
      });
    });
  });
})();

