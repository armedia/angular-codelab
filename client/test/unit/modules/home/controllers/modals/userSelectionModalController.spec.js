/* jshint undef:false*/
(function() {
  'use strict';

  describe('UserSelectionModalController', function() {
    var userSelectionModalController, rootScope, scope, $uibModalInstance, mockSessionService, mockConversationService;
    var mockLanguage = 'mk';
    var objectToSend = {currentLanguage: 'en', languageToBeCHanged: 'mk'};
    var mockConversations = {
      1: [{
        loggedUser: 'How are you today Vanessa?', timeLoggedUser: '12.30', user: 'Good, thank you. And  you?', timeUser: '12.32'
      }, {
        loggedUser: 'I am fine Vanessa, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
      }],
      2: [{
        loggedUser: 'How are you today Tom.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
      }, {
        loggedUser: 'I am fine Tom, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
      }]
    };
    var mockConversationsArray = [
      [{
        loggedUser: 'How are you today Vanessa?', timeLoggedUser: '12.30', user: 'Good, thank you. And  you?', timeUser: '12.32'
      }, {
        loggedUser: 'I am fine Vanessa, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
      }],
      [{
        loggedUser: 'How are you today Tom.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
      }, {
        loggedUser: 'I am fine Tom, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
      }]
    ];
    var mockId = 1;

    beforeEach(module('app'));
    beforeEach(module('home'));
    beforeEach(inject(function($rootScope, $controller, _SessionService_, _ConversationService_) {
      rootScope = $rootScope;
      scope = rootScope.$new();
      mockSessionService = _SessionService_;
      mockConversationService = _ConversationService_;
      spyOn(mockSessionService , 'getUser').and.returnValue('Armedia');
      spyOn(mockConversationService , 'getConversation').and.callFake(function(mockId) {
        return mockConversations[mockId];
      });
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'cancel']);
      userSelectionModalController = $controller('UserSelectionModalController', {
        $scope : scope,
        $uibModalInstance : $uibModalInstance,
        user: function() {
          return {
            name : "Test"
          };
        },
        mockSessionService : _SessionService_,
        mockConversationService : _ConversationService_,
        mockConversations : mockConversations
      });
    }));
    describe('getUser and getConversation functions',function(){
      it("should call getUser and getConversation functions", function() {
        userSelectionModalController.getLoggedUser();
        userSelectionModalController.getCurrentConversation(mockId);

        expect(mockSessionService.getUser).toHaveBeenCalled();
        expect(mockConversationService.getConversation).toHaveBeenCalledWith(mockId);
        expect(userSelectionModalController.currentConversation).toEqual([{
          loggedUser: 'How are you today Vanessa?', timeLoggedUser: '12.30', user: 'Good, thank you. And  you?', timeUser: '12.32'
        }, {
          loggedUser: 'I am fine Vanessa, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
        }]);
      });
    });
    describe('concatConversation function inside if statement',function(){
      beforeEach(function(){
        userSelectionModalController.enteredText = "test";
        userSelectionModalController.currentTime = "test date";
        userSelectionModalController.currentConversation = angular.copy(mockConversationsArray);
      });
      it("should call concatConversation and push new conversation", function() {
        expect(userSelectionModalController.currentConversation.length).toBe(2);

        userSelectionModalController.concatConversation();

        expect(userSelectionModalController.currentConversation.length).toBe(3);
      });
      afterEach(function(){
        userSelectionModalController.enteredText = "";
        userSelectionModalController.currentConversation = angular.copy(mockConversationsArray);
      });
    });
    describe('concatConversation function inside else statement',function(){
      beforeEach(function(){
        userSelectionModalController.enteredText = "";
        userSelectionModalController.currentTime = "test date";
        userSelectionModalController.currentConversation = angular.copy(mockConversationsArray);
      });
      it("should call concatConversation without pushing to new conversation", function() {
        expect(userSelectionModalController.currentConversation.length).toBe(2);

        userSelectionModalController.concatConversation();

        expect(userSelectionModalController.currentConversation.length).toBe(2);
      });
    });
    describe('changeLanguage function',function(){
      beforeEach(function(){
        spyOn(rootScope, '$broadcast');
      });
      it("should call changeLanguage function", function() {
        userSelectionModalController.currentLanguage = "en";
        userSelectionModalController.changeLanguage(mockLanguage);

        expect(rootScope.$broadcast).toHaveBeenCalledWith('changeLanguage', objectToSend);
      });
    });
    describe('dismissModal function',function(){
      it("should call dismissUserSelectionModal function", function() {
       userSelectionModalController.dismissUserSelectionModal();

       expect($uibModalInstance.close).toHaveBeenCalled();
      });
    });
  });
})();

