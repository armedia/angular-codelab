/* jshint undef:false*/
(function() {
  'use strict';

  describe('UserProfileModalController', function() {
    var userProfileModalController, rootScope, scope, $uibModalInstance;
    var mockLanguage = 'mk';
    var objectToSend = {currentLanguage: 'en', languageToBeCHanged: 'mk'};

    beforeEach(module('app'));
    beforeEach(module('home'));
    beforeEach(inject(function($rootScope, $controller) {
      rootScope = $rootScope;
      scope = rootScope.$new();
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
      userProfileModalController = $controller('UserProfileModalController', {
        $scope : scope,
        $uibModalInstance : $uibModalInstance,
        user: function() {
          return {
            name : "Test"
          };
        }
      });
    }));
    describe('changeLanguage function',function(){
      beforeEach(function(){
        spyOn(rootScope, '$broadcast');
      });
      it("should call changeLanguage function", function() {
        userProfileModalController.currentLanguage = "en";
        userProfileModalController.changeLanguage(mockLanguage);

        expect(rootScope.$broadcast).toHaveBeenCalledWith('changeLanguage', objectToSend);
      });
    });
    describe('dismissUserProfile function',function(){
      it("should call dismissUserProfileModal", function() {
        userProfileModalController.dismissUserProfileModal();

        expect($uibModalInstance.close).toHaveBeenCalled();
      });
    });
  });
})();

