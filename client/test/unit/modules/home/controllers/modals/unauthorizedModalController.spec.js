/* jshint undef:false*/
(function() {
  'use strict';

  describe('UnauthorizedModalController', function() {
    var unauthorizedModalController, rootScope, scope, $uibModalInstance;

    beforeEach(module('app'));
    beforeEach(module('home'));
    beforeEach(inject(function($rootScope, $controller) {
      rootScope = $rootScope;
      scope = rootScope.$new();
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
      unauthorizedModalController = $controller('UnauthorizedModalController', {
        $scope : scope,
        $uibModalInstance : $uibModalInstance
      });
    }));
    describe('confirmModal function',function(){
      it("should call confirmModal", function() {
        unauthorizedModalController.confirmModal();

        expect($uibModalInstance.close).toHaveBeenCalled();
      });
    });
  });
})();

