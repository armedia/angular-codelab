/* jshint undef:false*/
(function() {
  'use strict';

  describe('HomeCtrl', function() {
    var homeController, rootScope, q, state, mockDataService, mockSessionService, scope, modal;
    var mockLanguage = 'mk';
    var objectToSend = {currentLanguage: 'en', languageToBeCHanged: 'mk'};
    var mockupUsers = [

      {
        "id": 1,
        "icon": "assets/images/online.png",
        "name": "Vanessa Angel",
        "avatar": "assets/images/vanessa_avatar.png"
      },
      {
        "id": 2,
        "icon": "assets/images/online.png",
        "name": "Tom Baker",
        "avatar": "assets/images/tom_avatar.png"
      },
      {
        "id": 3,
        "icon": "assets/images/online.png",
        "name": "Eliza Bennett",
        "avatar": "assets/images/eliza_avatar.png"
      },
      {
        "id": 4,
        "icon": "assets/images/online.png",
        "name": "Martin Benson",
        "avatar": "assets/images/martin_avatar.png"
      },
      {
        "id": 5,
        "icon": "assets/images/online.png",
        "name": "Richard Briers",
        "avatar": "assets/images/richard_avatar.png"
      }
      ,
      {
        "id": 6,
        "icon": "assets/images/online.png",
        "name": "Kim Cattrall",
        "avatar": "assets/images/kim_avatar.png"
      }
      ,
      {
        "id": 7,
        "icon": "assets/images/online.png",
        "name": "Michael Crawford",
        "avatar": "assets/images/michael_avatar.png"
      }
      ,
      {
        "id": 8,
        "icon": "assets/images/online.png",
        "name": "Anthony Dawson",
        "avatar": "assets/images/anthony_avatar.png"
      }
    ];
    var fakeModal = {
      result: {
        then: function(confirmCallback) {
          //Store the callbacks for later when the user clicks on the OK button of the dialog
          this.confirmCallBack = confirmCallback;
        }
      },
      close: function( item ) {
        //The user clicked OK on the modal dialog, call the stored confirm callback with the selected item
        this.result.confirmCallBack();
      }
    };

    beforeEach(module('app', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));
    beforeEach(module('home'));
    beforeEach(inject(function($rootScope, $controller, $q, $state, $uibModal, _DataService_, _SessionService_) {
      rootScope = $rootScope;
      scope = rootScope.$new();
      q = $q;
      state = $state;
      modal = $uibModal;
      mockDataService = _DataService_;
      mockSessionService = _SessionService_;
      homeController = $controller('HomeCtrl', {
        $scope : scope,
        modal : $uibModal,
        mockDataService : _DataService_,
        mockSessionService : _SessionService_
      });
    }));
    describe('checkAuthentication and getUser success',function(){
      beforeEach(function(){
        spyOn(mockSessionService , 'isAuthenticated').and.returnValue(true);
        spyOn(mockDataService , 'getUsers').and.callFake(function() {
          var deferred = q.defer();
          deferred.resolve(mockupUsers);
          return deferred.promise;
        });
      });
      it("should call checkAuthentication with return value true and get users", function() {
        homeController.checkAuthentication();
        //homeController.getUsers();

        rootScope.$digest();

        expect(mockSessionService.isAuthenticated).toHaveBeenCalled();
        expect(mockDataService.getUsers).toHaveBeenCalled();
      });
    });
    describe('checkAuthentication success and getUsers fail',function(){
      beforeEach(function(){
        spyOn(mockSessionService , 'isAuthenticated').and.returnValue(true);
        spyOn(mockDataService , 'getUsers').and.callFake(function() {
          var deferred = q.defer();
          deferred.reject();
          return deferred.promise;
        });
        spyOn(modal, 'open').and.returnValue(fakeModal);
        spyOn(state, 'go');
      });
      it("should call checkAuthentication with return value true and fail for getting users", function() {
        var expectedState = 'root.login';
        homeController.checkAuthentication();
        //homeController.getUsers();

        rootScope.$digest();

        expect(mockSessionService.isAuthenticated).toHaveBeenCalled();
        expect(mockDataService.getUsers).toHaveBeenCalled();
        homeController.errorModalInstance.close();
        expect(state.go).toHaveBeenCalledWith(expectedState);
      });
    });
    describe('checkAuthentication error',function(){
      beforeEach(function(){
        spyOn(mockSessionService , 'isAuthenticated').and.returnValue(false);
        spyOn(modal, 'open').and.returnValue(fakeModal);
        spyOn(state, 'go');
      });
      it("should call checkAuthentication with return value false", function() {
        var expectedState = 'root.login';
        homeController.checkAuthentication();

        rootScope.$digest();

        expect(mockSessionService.isAuthenticated).toHaveBeenCalled();
        homeController.unauthorizedModalInstance.close();
        expect(state.go).toHaveBeenCalledWith(expectedState);
      });
    });
    describe('changeLanguage function',function(){
      beforeEach(function(){
        spyOn(rootScope, '$broadcast');
      });
      it("should call changeLanguage function", function() {
        homeController.currentLanguage = "en";
        homeController.changeLanguage(mockLanguage);

        expect(rootScope.$broadcast).toHaveBeenCalledWith('changeLanguage', objectToSend);
      });
    });
    describe('logOut function',function(){
      beforeEach(function(){
        spyOn(mockSessionService, 'setUser');
        spyOn(state, 'go');
      });
      it("should call logOut function", function() {
        var expectedState = 'root.login';
        homeController.logOut();

        rootScope.$digest();

        expect(mockSessionService.setUser).toHaveBeenCalled();
        expect(state.go).toHaveBeenCalledWith(expectedState);
      });
    });
  });
})();

