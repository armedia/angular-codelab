/* jshint undef:false*/
(function() {
  'use strict';

  describe('LoginCtrl', function() {
    var loginController, rootScope, q, state, mockLoginService, mockSessionService, scope;
    var mockupUser = {username: 'Armedia', password: 'Armedia123'};
    var mockupBadUser = {username: 'Armedia1', password: 'Armedia123'};
    var mockLanguage = 'mk';
    var objectToSend = {currentLanguage: 'en', languageToBeCHanged: 'mk'};

    beforeEach(module('app', function ($provide, $translateProvider) {

      $provide.factory('customLoader', function ($q) {
        return function () {
          var deferred = $q.defer();
          deferred.resolve({});
          return deferred.promise;
        };
      });

      $translateProvider.useLoader('customLoader');

    }));
    beforeEach(module('login'));
    beforeEach(inject(function($rootScope, $controller, $q, $state, _LoginService_, _SessionService_) {
      rootScope = $rootScope;
      scope = rootScope.$new();
      q = $q;
      state = $state;
      mockLoginService = _LoginService_;
      mockSessionService = _SessionService_;
      //spyOn(mockLoginService , 'userLogin').and.returnValue(q.when({}));
      loginController = $controller('LoginCtrl as loginvm', {
        $scope : scope,
        mockLoginService : _LoginService_,
        mockSessionService : _SessionService_
      });
    }));
    describe('loginUser with good credentials',function(){
      beforeEach(function(){
        spyOn(mockLoginService , 'userLogin').and.callFake(function(mockupUser) {
          var deferred = q.defer();
          if (mockupUser.username === "Armedia" && mockupUser.password === "Armedia123") {
            deferred.resolve(mockupUser);
          }
          else {
            deferred.reject();
          }
          return deferred.promise;
        });
        spyOn(state, 'go');
        spyOn(mockSessionService, 'setUser');
      });
      it("should call loginUser from LoginService with good credentials", function() {
        var expectedState = 'root.home';
        loginController.loginUser(mockupUser);

        rootScope.$digest();

        expect(mockLoginService.userLogin).toHaveBeenCalledWith(mockupUser);
        expect(mockSessionService.setUser).toHaveBeenCalled();
        expect(state.go).toHaveBeenCalledWith(expectedState);
      });
    });
    describe('changeLanguage function',function(){
      beforeEach(function(){
        spyOn(rootScope, '$broadcast');
      });
      it("should call changeLanguage function", function() {
        loginController.currentLanguage = "en";
        loginController.changeLanguage(mockLanguage);

        expect(rootScope.$broadcast).toHaveBeenCalledWith('changeLanguage', objectToSend);
      });
    });
    describe('loginUser with bad credentials',function(){
      beforeEach(function(){
        spyOn(mockLoginService , 'userLogin').and.callFake(function(mockupBadUser) {
          var deferred = q.defer();
            deferred.reject();

          return deferred.promise;
        });
      });
      it("should call loginUser from LoginService with bad credentials", function() {
        loginController.loginUser();

        rootScope.$digest();

        expect(mockLoginService.userLogin).toHaveBeenCalled();
      });
    });
  });
})();

