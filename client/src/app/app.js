(function() {
  'use strict';

  angular.element(document).ready(function() {
    angular.bootstrap(document, ['app']);
  });

  function config($stateProvider, $urlRouterProvider, $logProvider, $httpProvider, $translateProvider) {
    $urlRouterProvider.otherwise('/login');
    $logProvider.debugEnabled(true);
    $httpProvider.interceptors.push('httpInterceptor');
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: '/src/app/modules/{part}/resources/{lang}.json'
    });

    $translateProvider.preferredLanguage('en');
    $stateProvider
      .state('root', {
      });
  }

  function run($rootScope, $translate) {
    $rootScope.$on('changeLanguage', function (event, args) {
      var language = args.languageToBeCHanged;
      $translate.use(language);
    });
  }


  function MainCtrl($log) {
    $log.debug('MainCtrl laoded!');
  }

  angular.module('app', [
      'ui.router',
      'ui.bootstrap',
      'home',
      'login',
      'common',
      'templates',
      'pascalprecht.translate'
    ])
    .config(config)
    .run(run)
    .controller('MainCtrl', MainCtrl)
    .value('version', '1.1.0');
})();
