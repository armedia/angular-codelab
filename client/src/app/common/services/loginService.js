(function() {
    'use strict';

  /**
   * @ngdoc service
   * @name common.service: LoginService
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/common/services/loginService.js common/services/loginService.js}
   *
   * The LoginService used for login user logic.
   *
   * @requires $q
   *
   */
    angular
        .module('common')
        .factory('LoginService', loginService);

      function loginService($q) {

        /**
         * @ngdoc method
         * @name userLogin
         * @methodOf common.service: LoginService
         *
         * @description
         * Checking if username and password are valid credentials and if that is the case return success promise, else
         * return error promise
         *
         * @param {Object} user User object with username and password
         * @returns {Promise} returns a promise, success or error
         */
          function userLogin(user) {
            // Create  deferred object which will contain promise
              var deferred = $q.defer();

              if (user.username === 'Armedia' && user.password === 'Armedia123') {

                  //complete promise with resolve
                  deferred.resolve(user);
              }
              else {
                  //fail promise with reject
                  deferred.reject({success: false});
              }

              return deferred.promise;
          }

          return {
              userLogin: userLogin
          };
      }
})();
