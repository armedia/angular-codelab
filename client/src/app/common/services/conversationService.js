(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name common.service: ConversationService
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/common/services/conversationService.js common/services/conversationService.js}
   *
   * The ConversationService used for getting conversation for logged user and selected user
   *
   */
    angular
        .module('common')
        .factory('ConversationService', conversationService);

      function conversationService() {

        var conversations = {
          1: [{
            loggedUser: 'How are you today Vanessa?', timeLoggedUser: '12.30', user: 'Good, thank you. And  you?', timeUser: '12.32'
          }, {
            loggedUser: 'I am fine Vanessa, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
          }],
          2: [{
            loggedUser: 'How are you today Tom.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
          }, {
            loggedUser: 'I am fine Tom, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
          }],
          3: [{
            loggedUser: 'How are you today Eliza.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
          }, {
            loggedUser: 'I am fine Eliza, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
          }],
          4: [{
            loggedUser: 'How are you today Martin.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
          }, {
            loggedUser: 'I am fine Martin, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
          }],
          5: [{
            loggedUser: 'How are you today Richard.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
          }, {
            loggedUser: 'I am fine Richard, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
          }],
          6: [{
            loggedUser: 'How are you today Kim.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
          }, {
            loggedUser: 'I am fine Kim, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
          }],
          7: [{
            loggedUser: 'How are you today Michael.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
          }, {
            loggedUser: 'I am fine Michael, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
          }],
          8: [{
            loggedUser: 'How are you today Anthony.', timeLoggedUser: '12.30',user: 'Good, thank you. And  you?', timeUser: '12.32'
          }, {
            loggedUser: 'I am fine Anthony, tnx for asking?', timeLoggedUser: '12.32',user: 'Nice.', timeUser: '12.35'
          }]

        };

        /**
         * @ngdoc method
         * @name getConversation
         * @methodOf common.service: ConversationService
         *
         * @description
         * Returning conversation for selected user
         *
         * @param {Number} id Id for selected user
         * @returns {Array} returns array which holds conversation by selected user id
         */
        function getConversation(id) {

          return conversations[id];
        }

        return {
          getConversation: getConversation
        };
      }
})();
