(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name common.service: SessionService
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/common/services/sessionService.js common/services/sessionService.js}
   *
   * The SessionService used for setting and getting logged user
   *
   */
    angular
        .module('common')
        .factory('SessionService', sessionService);


      function sessionService() {

      /**
       * @ngdoc method
       * @name getUser
       * @methodOf common.service: SessionService
       *
       * @description
       * This method returns login user
       *
       * @returns {String} return username for logged user
       */
        function getUser() {
          return this.loggedUser.username;
        }

        /**
         * @ngdoc method
         * @name setUser
         * @methodOf common.service: SessionService
         *
         * @description
         * This method is setting logged user to `this` keyword
         *
         * @returns {Undefined} It doesn't return
         */
        function setUser(user) {

          this.loggedUser = user;
        }

        /**
         * @ngdoc method
         * @name isAuthenticated
         * @methodOf common.service: SessionService
         *
         * @description
         * This method returns true if user is logged in otherwise false
         *
         * @returns {Boolean} Return true/false
         */
        function isAuthenticated() {
          if (typeof this.loggedUser !== 'undefined' && this.loggedUser !== null) {
            return true;
          }
          else {
            return false;
          }

        }

        return {
          getUser: getUser,
          setUser: setUser,
          isAuthenticated: isAuthenticated
        };
      }
})();
