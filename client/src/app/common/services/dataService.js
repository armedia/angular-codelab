(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name common.service: DataService
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/common/services/dataService.js common/services/dataService.js}
   *
   * The DataService used for getting online users
   *
   * @requires $http
   * @requires $q
   */
    angular
        .module('common')
        .factory('DataService', dataService);

      function dataService($http, $q) {

      /**
       * @ngdoc method
       * @name getUsers
       * @methodOf common.service: DataService
       *
       * @description
       * Gets user list from assets folder in json format
       *
       * @returns {Promise} returns a promise, success or error
       */
        function getUsers() {

          return $http.get('assets/home/users.json').then(function (result){
            return result.data;

          }, function(err){

            return $q.reject(err);
          })
        }

        return {
          getUsers: getUsers
        };
      }

})();
