(function() {
  'use strict';

  /**
   * @ngdoc directive
   * @name home.directive: userProfile
   * @restrict E
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/modules/home/directives/user-profile/userProfile.directive.js home/directives/user-profile/userProfile.directive.js}
   *
   * The userProfile directive renders view for user profile
   *
   * @param {Object} user User data structure used fpr view rendering
   * @param {Object} parentModalInstance data structure used for view rendering
   *
   * @example
   <example>
       <file name="index.html">
           <user-profile user="user" parent-modal-instance="parentModalInstance">
           </user-profile>
       </file>
       <file name="app.js">
             angular.module('ngAppDemo', []).controller('userProfileDirectiveController', function($scope, $log, $uibModalInstance) {
                 $scope.user =
                    {
                      "id": 1,
                      "icon": "assets/images/online.png",
                      "name": "Vanessa Angel",
                      "avatar": "assets/images/vanessa_avatar.png"
                    };
                 $scope.parentModalInstance = $uibModalInstance;
             });
       </file>
   </example>
   */
  angular
    .module('home')
    .directive('userProfile', userProfile);

  userProfile.$inject = ['$uibModal'];

  /* @ngInject */
  function userProfile($uibModal) {
    var directive = {
      link: link,
      restrict: 'E',
      scope: {
        user: '=',
        parentModalInstance: '='
      },
      templateUrl: 'src/app/modules/home/directives/user-profile/userProfile.view.html'
    };
    return directive;

    function link(scope) {

      /**
       * @ngdoc method
       * @name openUserModal
       * @methodOf home.directive: userProfile
       *
       * @description
       * This method will open conversation modal between logged user and selected user
       *
       * @param {Object} selectedUser Object that contains information about selected user
       * @returns {undefined} It doesn't return
       */
      scope.openUserModal = function (selectedUser) {
        scope.parentModalInstance.dismiss('cancel');
        scope.userModalInstance = $uibModal.open({
          animation: true,
          size: 'lg',
          templateUrl: "src/app/modules/home/views/modals/userSelection.modal.view.html",
          controller: 'UserSelectionModalController as userselectionvm',
          backdrop: 'static',
          resolve: {
            user: function() {
              return selectedUser;
            }
          }
        });

        scope.userModalInstance.result.then(function() {});
      };

    }
  }

})();
