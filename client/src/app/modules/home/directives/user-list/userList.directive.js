(function() {
  'use strict';

  /**
   * @ngdoc directive
   * @name home.directive: userList
   * @restrict E
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/modules/home/directives/user-list/userList.directive.js home/directives/user-list/userList.directive.js}
   *
   * The userList directive renders simple list
   *
   * @param {Array} userList Data structure used fot list rendering
   *
   * @example
     <example>
        <file name="index.html">
           <user-list user-list="usersData">
           </user-list>
        </file>
        <file name="app.js">
             angular.module('ngAppDemo', []).controller('userListDirectiveController', function($scope, $log) {
                 $scope.usersData = [
                    {
                      "id": 1,
                      "icon": "assets/images/online.png",
                      "name": "Vanessa Angel",
                      "avatar": "assets/images/vanessa_avatar.png"
                    },
                    {
                      "id": 2,
                      "icon": "assets/images/online.png",
                      "name": "Tom Baker",
                      "avatar": "assets/images/tom_avatar.png"
                    }
                 ];
             });
        </file>
     </example>
   */
  angular
    .module('home')
    .directive('userList', userList);

  userList.$inject = ['$uibModal'];

  /* @ngInject */
  function userList($uibModal) {
    var directive = {
      link: link,
      restrict: 'E',
      scope: {
        userList: '='
      },
      templateUrl: 'src/app/modules/home/directives/user-list/userList.view.html'
    };
    return directive;

    function link(scope) {

      /**
       * @ngdoc method
       * @name openUserModal
       * @methodOf home.directive: userList
       *
       * @description
       * This method will open conversation modal between logged user and selected user
       *
       * @param {Object} selectedUser Object that contains information about selected user
       * @returns {undefined} It doesn't return
       */
      scope.openUserModal = function (selectedUser) {
        scope.userModalInstance = $uibModal.open({
          animation: true,
          size: 'lg',
          templateUrl: "src/app/modules/home/views/modals/userSelection.modal.view.html",
          controller: 'UserSelectionModalController as userselectionvm',
          backdrop: 'static',
          resolve: {
            user: function() {
              return selectedUser;
            }
          }
        });

        scope.userModalInstance.result.then(function() {});
      };

      /**
       * @ngdoc method
       * @name openUserProfile
       * @methodOf home.directive: userList
       *
       * @description
       * This method will open user profile modal for selected user
       *
       * @param {Object} selectedUser Object that contains information about selected user
       * @returns {undefined} It doesn't return
       */
      scope.openUserProfile = function (selectedUser) {
        scope.userProfileModalInstance = $uibModal.open({
          animation: true,
          size: 'lg',
          templateUrl: "src/app/modules/home/views/modals/userProfile.modal.view.html",
          controller: 'UserProfileModalController as userprofilevm',
          backdrop: 'static',
          resolve: {
            user: function() {
              return selectedUser;
            }
          }
        });

        scope.userProfileModalInstance.result.then(function() {});
      };
    }
  }

})();
