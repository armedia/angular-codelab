(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name home.controller: ErrorModalController
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/modules/home/controllers/modals/error.modal.controller.js home/controllers/modals/error.modal.controller.js}
   *
   * This controller holds logic when we are making call for getting user and that call return error .
   *
   * @requires $uibModalInstance
   */
  angular
    .module('home')
    .controller('ErrorModalController', ErrorModalController);

  ErrorModalController.$inject = ['$uibModalInstance'];

  /* @ngInject */
  function ErrorModalController($uibModalInstance) {

    /**
     * @ngdoc property
     * @name errorvm
     * @propertyOf home.controller: ErrorModalController
     *
     * @description
     * A named variable for the `this` keyword representing the ViewModel
     */
    var errorvm = this;

    errorvm.confirmModal = confirmModal;

    /**
     * @ngdoc method
     * @name confirmModal
     * @methodOf home.controller: ErrorModalController
     *
     * @description
     * This method will close error modal box.
     *
     * @returns {undefined} It doesn't return
     */
    function confirmModal () {
      $uibModalInstance.close();
    }
  }
})();
