(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name home.controller: UserSelectionModalController
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/modules/home/controllers/modals/userSelection.modal.controller.js home/controllers/modals/userSelection.modal.controller.js}
   *
   * This controller holds logic about conversation between logged user and selected user.
   *
   * @requires $uibModalInstance
   * @requires $rootScope
   * @requires $translate
   * @requires SessionService
   * @requires user
   * @requires ConversationService
   */
  angular
    .module('home')
    .controller('UserSelectionModalController', UserSelectionModalController);

  UserSelectionModalController.$inject = ['$uibModalInstance', '$rootScope','$translate', 'SessionService', 'user', 'ConversationService'];

  /* @ngInject */
  function UserSelectionModalController($uibModalInstance, $rootScope, $translate, sessionService, user, conversationService) {

    /**
     * @ngdoc property
     * @name userselectionvm
     * @propertyOf home.controller: UserSelectionModalController
     *
     * @description
     * A named variable for the `this` keyword representing the ViewModel
     */
    var userselectionvm = this;

    userselectionvm.currentTime = new Date().getTime();
    userselectionvm.currentLanguage = $translate.use();
    userselectionvm.user = user;
    userselectionvm.enteredText = "";

    userselectionvm.dismissUserSelectionModal = dismissUserSelectionModal;
    userselectionvm.concatConversation = concatConversation;
    userselectionvm.getCurrentConversation = getCurrentConversation;
    userselectionvm.getLoggedUser = getLoggedUser;
    userselectionvm.changeLanguage = changeLanguage;

    getCurrentConversation(userselectionvm.user.id);
    getLoggedUser();

    /**
     * @ngdoc method
     * @name getCurrentConversation
     * @methodOf home.controller: UserSelectionModalController
     *
     * @description
     * This method will make service call for getting conversation for selected user and logged user
     *
     * @param {number} userId Id for selected user from user list
     * @returns {undefined} It doesn't return
     */
    function getCurrentConversation (userId) {
      userselectionvm.currentConversation = conversationService.getConversation(userId);
    }

    /**
     * @ngdoc method
     * @name getLoggedUser
     * @methodOf home.controller: UserSelectionModalController
     *
     * @description
     * This method will make service call for getting logged user
     *
     * @returns {undefined} It doesn't return
     */
    function getLoggedUser () {
      userselectionvm.loggedUser = sessionService.getUser();
    }

    /**
     * @ngdoc method
     * @name concatConversation
     * @methodOf home.controller: UserSelectionModalController
     *
     * @description
     * This method will update existing conversation for selected user and logged user
     * with currently typed conversation between them
     *
     * @returns {undefined} It doesn't return
     */
    function concatConversation () {

      if (userselectionvm.enteredText && userselectionvm.enteredText !== '') {
        userselectionvm.currentConversation.push({
          loggedUser: userselectionvm.enteredText,
          timeLoggedUser: userselectionvm.currentTime,
          user: 'typing...',
          timeUser: ""
        });
        userselectionvm.enteredText = "";
      }
      else {
        userselectionvm.enteredText = "";
      }
    }

    /**
     * @ngdoc method
     * @name dismissUserSelectionModal
     * @methodOf home.controller: UserSelectionModalController
     *
     * @description
     * This method will close user selection modal
     *
     * @returns {undefined} It doesn't return
     */
    function dismissUserSelectionModal () {
      $uibModalInstance.close();
    }

    function changeLanguage(language) {
      var objectToSend = {};
      objectToSend.currentLanguage = userselectionvm.currentLanguage;
      objectToSend.languageToBeCHanged = language;
      $rootScope.$broadcast('changeLanguage', objectToSend);
    }
  }
})();
