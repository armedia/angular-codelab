(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name home.controller: UnauthorizedModalController
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/modules/home/controllers/modals/unauthorized.modal.controller.js home/controllers/modals/unauthorized.modal.controller.js}
   *
   * This controller holds logic about showing unauthorized modal if user is not logged in.
   *
   * @requires $uibModalInstance
   */
  angular
    .module('home')
    .controller('UnauthorizedModalController', UnauthorizedModalController);

  UnauthorizedModalController.$inject = ['$uibModalInstance'];

  /* @ngInject */
  function UnauthorizedModalController($uibModalInstance) {

    /**
     * @ngdoc property
     * @name unauthvm
     * @propertyOf home.controller: UnauthorizedModalController
     *
     * @description
     * A named variable for the `this` keyword representing the ViewModel
     */
    var unauthvm = this;

    unauthvm.confirmModal = confirmModal;

    /**
     * @ngdoc method
     * @name confirmModal
     * @methodOf home.controller: UnauthorizedModalController
     *
     * @description
     * This method will close unauthorized modal box.
     *
     * @returns {undefined} It doesn't return
     */
    function confirmModal () {
      $uibModalInstance.close();
    }
  }
})();
