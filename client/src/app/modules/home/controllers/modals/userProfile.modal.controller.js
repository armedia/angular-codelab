(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name home.controller: UserProfileModalController
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/modules/home/controllers/modals/userProfile.modal.controller.js home/controllers/modals/userProfile.modal.controller.js}
   *
   * This controller holds logic for showing profile for selected user.
   *
   * @requires $rootScope
   * @requires $translate
   * @requires $uibModalInstance
   * @requires user
   */
  angular
    .module('home')
    .controller('UserProfileModalController', UserProfileModalController);

  UserProfileModalController.$inject = ['$rootScope', '$translate', '$uibModalInstance', 'user'];

  /* @ngInject */
  function UserProfileModalController($rootScope, $translate, $uibModalInstance, user) {

    /**
     * @ngdoc property
     * @name userprofilevm
     * @propertyOf home.controller: UserProfileModalController
     *
     * @description
     * A named variable for the `this` keyword representing the ViewModel
     */
    var userprofilevm = this;

    userprofilevm.user = user;
    userprofilevm.currentLanguage = $translate.use();
    userprofilevm.modalInstance = $uibModalInstance;
    userprofilevm.dismissUserProfileModal = dismissUserProfileModal;
    userprofilevm.changeLanguage = changeLanguage;

    /**
     * @ngdoc method
     * @name dismissUserProfileModal
     * @methodOf home.controller: UserProfileModalController
     *
     * @description
     * This method will close user profile modal
     *
     * @returns {undefined} It doesn't return
     */
    function dismissUserProfileModal () {
      $uibModalInstance.close();
    }

    /**
     * @ngdoc method
     * @name changeLanguage
     * @methodOf home.controller: UserProfileModalController
     *
     * @description
     * This method will send $broadcast event with object that contains current language and
     * language that need to be change to
     *
     * @param {String} language Language that app need to be translated to
     * @returns {undefined} It doesn't return
     */
    function changeLanguage(language) {
      var objectToSend = {};
      objectToSend.currentLanguage = userprofilevm.currentLanguage;
      objectToSend.languageToBeCHanged = language;
      $rootScope.$broadcast('changeLanguage', objectToSend);
    }
  }
})();
