(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name home.controller: HomeCtrl
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/modules/home/controllers/home.controller.js home/controllers/home.controller.js}
   *
   * This controller is responsible for showing home page, logged in users.
   *
   * @requires $rootScope
   * @requires $state
   * @requires $uibModal
   * @requires $translate
   * @requires SessionService
   * @requires DataService
   *
   */
  angular
    .module('home')
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = ['$rootScope', '$state', '$uibModal', '$translate', 'SessionService', 'DataService'];

  /* @ngInject */
  function HomeCtrl($rootScope, $state, $uibModal, $translate, sessionService, dataService) {

    /**
     * @ngdoc property
     * @name homevm
     * @propertyOf home.controller: HomeCtrl
     *
     * @description
     * A named variable for the `this` keyword representing the ViewModel
     */
    var homevm = this;

    homevm.logOut = logOut;
    homevm.checkAuthentication = checkAuthentication;
    homevm.changeLanguage = changeLanguage;

    homevm.currentLanguage = $translate.use();

    checkAuthentication();

    /**
     * @ngdoc method
     * @name checkAuthentication
     * @methodOf home.controller: HomeCtrl
     *
     * @description
     * This method will make service call to check if user is logged. If logged, get users otherwise show unauthorized modal
     * which will redirect to login page.
     *
     * @returns {undefined} It doesn't return
     */
    function checkAuthentication () {
      if (sessionService.isAuthenticated()) {
        getUsers();
      }
      else {
        homevm.unauthorizedModalInstance = $uibModal.open({
          animation: true,
          size: 'lg',
          templateUrl: "src/app/modules/home/views/modals/unauthorized.modal.view.html",
          controller: 'UnauthorizedModalController as unauthvm',
          backdrop: 'static',
          keyboard: false,
          resolve: {
          }
        });

        homevm.unauthorizedModalInstance.result.then(function() {
          $state.go('root.login');
        });
      }
    }

    /**
     * @ngdoc method
     * @name getUsers
     * @methodOf home.controller: HomeCtrl
     *
     * @description
     * This method will make service call for getting user list.
     *
     * @returns {undefined} It doesn't return
     */
    function getUsers () {
      dataService.getUsers().then(function (users){
        homevm.users = users;
      }, function (error){
          homevm.errorModalInstance = $uibModal.open({
            animation: true,
            size: 'lg',
            templateUrl: "src/app/modules/home/views/modals/error.modal.view.html",
            controller: 'ErrorModalController as errorvm',
            backdrop: 'static',
            resolve: {
            }
          });

          homevm.errorModalInstance.result.then(function() {
            $state.go('root.login');
          });
      });
    }

    /**
     * @ngdoc method
     * @name changeLanguage
     * @methodOf home.controller: HomeCtrl
     *
     * @description
     * This method will send $broadcast event with object that contains current language and
     * language that need to be change to
     *
     * @param {String} language Language that app need to be translated to
     * @returns {undefined} It doesn't return
     */
    function changeLanguage(language) {
      var objectToSend = {};
      objectToSend.currentLanguage = homevm.currentLanguage;
      objectToSend.languageToBeCHanged = language;
      $rootScope.$broadcast('changeLanguage', objectToSend);
    }

    /**
     * @ngdoc method
     * @name logOut
     * @methodOf home.controller: HomeCtrl
     *
     * @description
     * This method will log out user and redirect to login page.
     *
     * @returns {undefined} It doesn't return
     */
    function logOut () {
      sessionService.setUser(null);
      $state.go('root.login');
    }
  }
})();
