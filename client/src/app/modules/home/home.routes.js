(function() {
  'use strict';

  function config($stateProvider) {
    $stateProvider
      .state('root.home', {
        url: '/home',
        views: {
          '@': {
            templateUrl: 'src/app/modules/home/views/home.view.html',
            controller: 'HomeCtrl as homevm'
          }
        },
        resolve: {
          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart('home');
            return $translate.refresh();
          }]
        }
      });
  }

  angular.module('home')
    .config(config);
})();
