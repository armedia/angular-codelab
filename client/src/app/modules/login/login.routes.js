(function() {
    'use strict';

    function config($stateProvider) {
        $stateProvider
          .state('root.login', {
              url: '/login',
              views: {
                  '@': {
                      templateUrl: 'src/app/modules/login/views/login.view.html',
                      controller: 'LoginCtrl as loginvm'
                  }
              },
              resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                  $translatePartialLoader.addPart('login');
                  return $translate.refresh();
                }]
              }
          });
    }

    angular.module('login')
        .config(config);
})();
