(function() {
  'use strict';

  /**
   * @ngdoc controller
   * @name login.controller: LoginCtrl
   *
   * @description
   *
   * {@link https://gitlab.armedia.com/solutions-engineering/angular-codelab/blob/master/client/src/app/modules/login/controllers/login.controller.js login/controllers/login.controller.js}
   *
   * This controller is responsible for showing login form and contains logic for providing
   * username and password for login service
   *
   * @requires $rootScope
   * @requires $translate
   * @requires $state
   * @requires LoginService
   * @requires SessionService
   *
   */
    angular
            .module('login')
            .controller('LoginCtrl', LoginCtrl);

        LoginCtrl.$inject = ['$rootScope', '$translate', '$state', 'LoginService', 'SessionService'];

        function LoginCtrl($rootScope, $translate, $state, loginService, sessionService) {

          /**
           * @ngdoc property
           * @name loginvm
           *
           * @propertyOf login.controller: LoginCtrl
           *
           * @description
           * A named variable for the `this` keyword representing the ViewModel
           */
            var loginvm = this;

            loginvm.user = {};
            loginvm.hasError = false;
            loginvm.loginUser = loginUser;
            loginvm.changeLanguage = changeLanguage;

            loginvm.currentLanguage = $translate.use();

          /**
           * @ngdoc method
           * @name changeLanguage
           * @methodOf login.controller: LoginCtrl
           *
           * @description
           * This method will send $broadcast event with object that contains current language and
           * language that need to be change to
           *
           * @param {String} language Language that app need to be translated to
           * @returns {undefined} It doesn't return
           */
            function changeLanguage(language) {
              var objectToSend = {};
              objectToSend.currentLanguage = loginvm.currentLanguage;
              objectToSend.languageToBeCHanged = language;
              $rootScope.$broadcast('changeLanguage', objectToSend);
            }

          /**
           * @ngdoc method
           * @name loginUser
           * @methodOf login.controller: LoginCtrl
           *
           * @description
           * This method will send user object to login service with good or bad credentials
           *
           * @param {object} user User object with username and password that needs to be sent
           * @returns {undefined} It doesn't return
           */
            function loginUser (user) {
                loginService.userLogin(user).then(function(response) {
                        sessionService.setUser(response);
                        loginvm.hasError = false;
                        $state.go('root.home');
                    }, function(error) {
                        loginvm.hasError = true;
                 });

            }
        }
})();
